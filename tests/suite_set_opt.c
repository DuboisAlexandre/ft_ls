/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   suite_set_opt.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/26 18:00:02 by adubois           #+#    #+#             */
/*   Updated: 2016/04/26 21:15:47 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "test.h"

static void		test_null_ptr(void)
{
	v_assert(ft_ls_set_opt(NULL, '1') == -1);
	v_test_success("null ptr");
}

static void		test_null_opt(void)
{
	t_ls_options	opts;

	opts = 0;
	v_assert(ft_ls_set_opt(&opts, '\0') == -1);
	v_test_success("null opt");
}

static void		test_null_ptr_and_opt(void)
{
	v_assert(ft_ls_set_opt(NULL, '\0') == -1);
	v_test_success("null ptr and opt");
}

static void		test_invalid_opt(void)
{
	t_ls_options	opts;

	opts = 0;
	v_assert(ft_ls_set_opt(&opts, 'm') == -1);
	v_test_success("invalid opt");
}

static void		test_opt_1(void)
{
	t_ls_options	opts;

	opts = 0;
	v_assert(ft_ls_set_opt(&opts, '1') == 0 && opts == 0x80000000);
	v_test_success("opt 1");
}

static void		test_opt_A(void)
{
	t_ls_options	opts;

	opts = 0;
	v_assert(ft_ls_set_opt(&opts, 'A') == 0 && opts == 0x40000000);
	v_test_success("opt A");
}

static void		test_opt_a(void)
{
	t_ls_options	opts;

	opts = 0;
	v_assert(ft_ls_set_opt(&opts, 'a') == 0 && opts == 0x20000000);
	v_test_success("opt a");
}

static void		test_opt_C(void)
{
	t_ls_options	opts;

	opts = 0;
	v_assert(ft_ls_set_opt(&opts, 'C') == 0 && opts == 0x10000000);
	v_test_success("opt C");
}

static void		test_opt_c(void)
{
	t_ls_options	opts;

	opts = 0;
	v_assert(ft_ls_set_opt(&opts, 'c') == 0 && opts == 0x8000000);
	v_test_success("opt c");
}

static void		test_opt_d(void)
{
	t_ls_options	opts;

	opts = 0;
	v_assert(ft_ls_set_opt(&opts, 'd') == 0 && opts == 0x4000000);
	v_test_success("opt d");
}

static void		test_opt_1_and_d(void)
{
	t_ls_options	opts;

	opts = 0;
	v_assert(ft_ls_set_opt(&opts, 'd') == 0 && ft_ls_set_opt(&opts, '1') == 0 && opts == 0x84000000);
	v_test_success("opt 1 and d");
}

static void		test_opt_all(void)
{
	t_ls_options	opts;
	int				total;

	total = 0;
	opts = 0;
	total += ft_ls_set_opt(&opts, '1');
	total += ft_ls_set_opt(&opts, 'A');
	total += ft_ls_set_opt(&opts, 'a');
	total += ft_ls_set_opt(&opts, 'C');
	total += ft_ls_set_opt(&opts, 'c');
	total += ft_ls_set_opt(&opts, 'd');
	total += ft_ls_set_opt(&opts, 'F');
	total += ft_ls_set_opt(&opts, 'f');
	total += ft_ls_set_opt(&opts, 'G');
	total += ft_ls_set_opt(&opts, 'g');
	total += ft_ls_set_opt(&opts, 'H');
	total += ft_ls_set_opt(&opts, 'h');
	total += ft_ls_set_opt(&opts, 'i');
	total += ft_ls_set_opt(&opts, 'k');
	total += ft_ls_set_opt(&opts, 'L');
	total += ft_ls_set_opt(&opts, 'l');
	total += ft_ls_set_opt(&opts, 'n');
	total += ft_ls_set_opt(&opts, 'o');
	total += ft_ls_set_opt(&opts, 'P');
	total += ft_ls_set_opt(&opts, 'p');
	total += ft_ls_set_opt(&opts, 'q');
	total += ft_ls_set_opt(&opts, 'R');
	total += ft_ls_set_opt(&opts, 'r');
	total += ft_ls_set_opt(&opts, 'S');
	total += ft_ls_set_opt(&opts, 's');
	total += ft_ls_set_opt(&opts, 'T');
	total += ft_ls_set_opt(&opts, 't');
	total += ft_ls_set_opt(&opts, 'u');
	total += ft_ls_set_opt(&opts, 'w');
	v_assert(total == 0 && opts == 0xFFFFFFF8);
	v_test_success("opt all");
}

void	suite_set_opt(void)
{
	test_null_ptr();
	test_null_opt();
	test_null_ptr_and_opt();
	test_invalid_opt();
	test_opt_1();
	test_opt_A();
	test_opt_a();
	test_opt_C();
	test_opt_c();
	test_opt_d();
	test_opt_1_and_d();
	test_opt_all();

	v_suite_success("set_opt");
}
