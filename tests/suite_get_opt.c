/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   suite_get_opt.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/26 19:32:24 by adubois           #+#    #+#             */
/*   Updated: 2016/04/26 21:06:53 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "test.h"

static void		test_null_opt(void)
{
	t_ls_options	opts;

	opts = 0;
	v_assert(ft_ls_get_opt(opts, '\0') == -1);
	v_test_success("null opt");
}

static void		test_invalid_opt(void)
{
	t_ls_options	opts;

	opts = 0;
	v_assert(ft_ls_get_opt(opts, 'm') == -1);
	v_test_success("invalid opt");
}

static void		test_opt_1_true(void)
{
	t_ls_options	opts;

	opts = 0x80000000;
	v_assert(ft_ls_get_opt(opts, '1') == 1);
	v_test_success("opt 1 true");
}

static void		test_opt_1_false(void)
{
	t_ls_options	opts;

	opts = 0x40000000;
	v_assert(ft_ls_get_opt(opts, '1') == 0);
	v_test_success("opt 1 false");
}

static void		test_opt_d_true(void)
{
	t_ls_options	opts;

	opts = 0x4000000;
	v_assert(ft_ls_get_opt(opts, 'd') == 1);
	v_test_success("opt d true");
}

static void		test_opt_d_false(void)
{
	t_ls_options	opts;

	opts = 0x8000000;
	v_assert(ft_ls_get_opt(opts, 'd') == 0);
	v_test_success("opt d false");
}

static void		test_opt_1_and_d_true(void)
{
	t_ls_options	opts;

	opts = 0x84000000;
	v_assert(ft_ls_get_opt(opts, 'd') + ft_ls_get_opt(opts, '1') == 2);
	v_test_success("opt 1 and d true");
}

static void		test_opt_1_and_d_false_1(void)
{
	t_ls_options	opts;

	opts = 0x44000000;
	v_assert(ft_ls_get_opt(opts, 'd') + ft_ls_get_opt(opts, '1') == 1);
	v_test_success("opt 1 and d false 1");
}

static void		test_opt_1_and_d_false_d(void)
{
	t_ls_options	opts;

	opts = 0x88000000;
	v_assert(ft_ls_get_opt(opts, 'd') + ft_ls_get_opt(opts, '1') == 1);
	v_test_success("opt 1 and d false d");
}

static void		test_opt_1_and_d_false(void)
{
	t_ls_options	opts;

	opts = 0x48000000;
	v_assert(ft_ls_get_opt(opts, 'd') + ft_ls_get_opt(opts, '1') == 0);
	v_test_success("opt 1 and d false");
}

static void		test_opt_all(void)
{
	t_ls_options	opts;
	int				total;

	total = 0;
	opts = 0xFFFFFFF8;
	total += ft_ls_get_opt(opts, '1');
	total += ft_ls_get_opt(opts, 'A');
	total += ft_ls_get_opt(opts, 'a');
	total += ft_ls_get_opt(opts, 'C');
	total += ft_ls_get_opt(opts, 'c');
	total += ft_ls_get_opt(opts, 'd');
	total += ft_ls_get_opt(opts, 'F');
	total += ft_ls_get_opt(opts, 'f');
	total += ft_ls_get_opt(opts, 'G');
	total += ft_ls_get_opt(opts, 'g');
	total += ft_ls_get_opt(opts, 'H');
	total += ft_ls_get_opt(opts, 'h');
	total += ft_ls_get_opt(opts, 'i');
	total += ft_ls_get_opt(opts, 'k');
	total += ft_ls_get_opt(opts, 'L');
	total += ft_ls_get_opt(opts, 'l');
	total += ft_ls_get_opt(opts, 'n');
	total += ft_ls_get_opt(opts, 'o');
	total += ft_ls_get_opt(opts, 'P');
	total += ft_ls_get_opt(opts, 'p');
	total += ft_ls_get_opt(opts, 'q');
	total += ft_ls_get_opt(opts, 'R');
	total += ft_ls_get_opt(opts, 'r');
	total += ft_ls_get_opt(opts, 'S');
	total += ft_ls_get_opt(opts, 's');
	total += ft_ls_get_opt(opts, 'T');
	total += ft_ls_get_opt(opts, 't');
	total += ft_ls_get_opt(opts, 'u');
	total += ft_ls_get_opt(opts, 'w');
	v_assert(total == 29);
	v_test_success("opt all");
}

void	suite_get_opt(void)
{
	test_null_opt();
	test_invalid_opt();
	test_opt_1_true();
	test_opt_1_false();
	test_opt_d_true();
	test_opt_d_false();
	test_opt_1_and_d_true();
	test_opt_1_and_d_false_1();
	test_opt_1_and_d_false_d();
	test_opt_1_and_d_false();
	test_opt_all();

	v_suite_success("get_opt");
}
