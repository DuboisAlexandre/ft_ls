/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/26 18:21:01 by adubois           #+#    #+#             */
/*   Updated: 2016/04/26 21:10:43 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TEST_H
# define TEST_H

#include "vittf.h"
#include "ft_ls.h"

void	suite_set_opt(void);
void	suite_get_opt(void);

#endif
