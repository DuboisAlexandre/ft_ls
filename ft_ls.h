/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/26 18:35:48 by adubois           #+#    #+#             */
/*   Updated: 2016/04/26 21:09:56 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_LS_H
# define FT_LS_H

# include "libft.h"

# define FT_LS_OPTIONS "1AaCcdFfGgHhikLlnoPpqRrSsTtuw"

typedef unsigned int	t_ls_options;

int						ft_ls_set_opt(t_ls_options *opts, char opt);
int						ft_ls_get_opt(t_ls_options opts, char opt);

#endif
