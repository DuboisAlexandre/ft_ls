/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls_options.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adubois <adubois@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/26 18:35:59 by adubois           #+#    #+#             */
/*   Updated: 2016/04/26 21:17:57 by adubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

int		ft_ls_set_opt(t_ls_options *opts, char opt)
{
	static char		*options = FT_LS_OPTIONS;
	int				pos;

	if (opts == NULL || opt == '\0')
		return (-1);
	if ((pos = ft_strchrpos(options, opt)) == -1)
		return (-1);
	*opts |= 1 << (31 - (unsigned int)pos);
	return (0);
}

int		ft_ls_get_opt(t_ls_options opts, char opt)
{
	static char		*options = FT_LS_OPTIONS;
	int				pos;

	if (opt == '\0')
		return (-1);
	if ((pos = ft_strchrpos(options, opt)) == -1)
		return (-1);
	
	return (opts >> (31 - (unsigned int)pos) & 0x1);
}
